//package io.payo.product.dto;
//
//import io.payo.product.models.Product;
//import org.mapstruct.Mapper;
//import org.mapstruct.Mapping;
//import org.mapstruct.factory.Mappers;
//
//@Mapper
//public interface ProductMapper {
//    ProductMapper MAPPER = Mappers.getMapper(ProductMapper.class);
//    @Mapping(source = "id", target = "id")
//    @Mapping(source = "name", target = "name")
//    ProductDto toDto(Product product);
//}
