package io.payo.product.dto;

import io.payo.product.models.Product;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class ProductDto implements Serializable {
    private int id;
    private String name;

    public static ProductDto map(Product product){
        return ProductDto.builder()
                .id(product.getId())
                .name(product.getName())
                .build();
    }
}
