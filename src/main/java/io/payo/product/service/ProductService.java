package io.payo.product.service;

import io.payo.product.dto.ProductDto;
import io.payo.product.models.Product;
import io.payo.product.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang.SerializationUtils;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProductService {
    @Value("${topic.name}")
    private String productTopic;
    @Value("${exchange.name}")
    private String productExchange;

    private final ProductRepository productRepository;
    private final RabbitTemplate rabbitTemplate;

    public void order() {
        Product product = Product.builder().name("Iphone").build();
        Product saved = productRepository.save(product);
        ProductDto dto = ProductDto.map(saved);
        byte[] body = SerializationUtils.serialize(dto);
        Message message = new Message(body);
        rabbitTemplate.send(productExchange, "products."+dto.getId(), message);
    }
}
